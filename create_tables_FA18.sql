DROP TABLE IF EXISTS TERMS;
DROP TABLE IF EXISTS MAJORS_FA18;
DROP TABLE IF EXISTS COURSES_FA18;
CREATE TABLE TERMS(term_string VARCHAR(20),
					term_id INT NOT NULL PRIMARY KEY,
					CONSTRAINT unique_term UNIQUE(term_string));
CREATE TABLE MAJORS_FA18(abbrev VARCHAR(10) PRIMARY KEY,
						major VARCHAR(60) NOT NULL,
						CONSTRAINT unique_major UNIQUE(major));
CREATE TABLE COURSES_FA18(crn INT PRIMARY KEY NOT NULL,
						department VARCHAR(60) NOT NULL,
						course_number INT NOT NULL,
						section_number INT NOT NULL,
						campus VARCHAR(30),
						credit_hours_minimum REAL,
						credit_hours_maximum REAL,
						title VARCHAR(60) NOT NULL,
						days VARCHAR(10),
						time_start VARCHAR(10) NOT NULL,
						time_end VARCHAR(10) NOT NULL,
						capacity_seats INT NOT NULL,
						filled_seats INT NOT NULL,
						remaining_seats INT NOT NULL,
						capacity_waitlist INT,
						filled_waitlist INT,
						remaining_waitlist INT,
						capacity_crosslist INT,
						filled_crosslist INT,
						remaining_crosslist INT,
						instructors VARCHAR(100),
						date_start DATE,
						date_end DATE,
						location VARCHAR(15) NOT NULL,
						type VARCHAR(20),
						links VARCHAR(200),
						prerequisites VARCHAR(200),
						notes VARCHAR(600),
						division VARCHAR(20),
						term VARCHAR(20) NOT NULL,
						FOREIGN KEY (department) REFERENCES MAJORS_FA18(abbrev),
						FOREIGN KEY (term) REFERENCES TERMS(term_string));

.read majors_FA18.sql
.read terms_FA18.sql
.read courses_FA18.sql