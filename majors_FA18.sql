INSERT INTO MAJORS_FA18 (abbrev, major) VALUES ("ENGT", "Engineering Technology"),
("CHM", "Chemistry"),
("TLI", "Technology Ldrshp Innovatn"),
("REG", "Reg File Maintenance"),
("HDFS", "Human Dev &amp;Family Studies"),
("GS", "General Studies"),
("LING", "Linguistics"),
("BIOL", "Biological Sciences"),
("AGR", "Agriculture"),
("VM", "Veterinary Medicine"),
("CMPL", "Comparative Literature"),
("SCLA", "Studies Coll Liberal Arts"),
("FNR", "Forestry&amp;Natural Resources"),
("IE", "Industrial Engineering"),
("AT", "Aviation Technology"),
("MA", "Mathematics"),
("EPCS", "Engr Proj Cmity Service"),
("NS", "Naval Science"),
("REL", "Religious Studies"),
("IT", "Industrial Technology"),
("VCS", "Veterinary Clinical Sci"),
("AAS", "African American Studies"),
("ANSC", "Animal Sciences"),
("DANC", "Dance"),
("AFT", "Aerospace Studies"),
("PTEC", "Polytechnic"),
("IDE", "Interdisciplinary Engr"),
("HIST", "History"),
("CM", "Construction Management"),
("SPAN", "Spanish"),
("NUCL", "Nuclear Engineering"),
("AAE", "Aero &amp; Astro Engineering"),
("BTNY", "Botany &amp; Plant Pathology"),
("BME", "Biomedical Engineering"),
("PHIL", "Philosophy"),
("ASAM", "Asian American Studies"),
("JWST", "Jewish Studies"),
("CLPH", "Clinical Pharmacy"),
("EEE", "Environ &amp; Ecological Engr"),
("ANTH", "Anthropology"),
("FR", "French"),
("OBHR", "Orgnztnl Bhvr &amp;Hum Resrce"),
("NUR", "Nursing"),
("BAND", "Bands"),
("ENTR", "Entrepreneurship"),
("GER", "German"),
("CPB", "Comparative Pathobiology"),
("MSE", "Materials Engineering"),
("FVS", "Film And Video Studies"),
("EAPS", "Earth Atmos Planetary Sci"),
("SYS", "Purdue Sys Collaboratory"),
("ASM", "Agricultural Systems Mgmt"),
("CSR", "Consumer Science"),
("CE", "Civil Engineering"),
("WGSS", "Women Gend&amp;Sexuality Std"),
("MSL", "Military Science &amp; Ldrshp"),
("GEP", "Global Engineering Program"),
("CAND", "Candidate"),
("HONR", "Honors"),
("NUTR", "Nutrition Science"),
("HSCI", "Health Sciences"),
("SA", "Study Abroad"),
("PHAD", "Pharmacy Administration"),
("MCMP", "Med Chem &amp;Molecular Pharm"),
("NUPH", "Nuclear Pharmacy"),
("STAT", "Statistics"),
("ENE", "Engineering Education"),
("PSY", "Psychological Sciences"),
("HORT", "Horticulture"),
("SCI", "General Science"),
("ENTM", "Entomology"),
("ECON", "Economics"),
("ENGL", "English"),
("EDCI", "Educ-Curric &amp; Instruction"),
("GREK", "Greek"),
("LATN", "Latin"),
("MUS", "Music History &amp; Theory"),
("BMS", "Basic Medical Sciences"),
("CHNS", "Chinese"),
("PTGS", "Portuguese"),
("LA", "Landscape Architecture"),
("ENGR", "Engineering"),
("HEBR", "Hebrew"),
("EDST", "Ed Leadrship&amp;Cultrl Fnd"),
("AMST", "American Studies"),
("AGRY", "Agronomy"),
("IDIS", "Interdisciplinary Studies"),
("ABE", "Agri &amp; Biol Engineering"),
("FS", "Food Science"),
("MARS", "Medieval &amp;Renaissance Std"),
("PES", "Physical Education Skills"),
("TECH", "Technology"),
("POL", "Political Science"),
("RUSS", "Russian"),
("HHS", "College Health &amp; Human Sci"),
("ASTR", "Astronomy"),
("EDPS", "Educ-Ed&#x27;l and Psy Studies"),
("CHE", "Chemical Engineering"),
("CLCS", "Classics"),
("MGMT", "Management"),
("MET", "Mechanical Engr Tech"),
("CEM", "Construction Engr &amp; Mgmt"),
("PHPR", "Pharmacy Practice"),
("COM", "Communication"),
("ILS", "Information &amp; Library Sci"),
("CDIS", "Critical Disability Std"),
("CS", "Computer Sciences"),
("IPPH", "Industrial &amp; Phys Pharm"),
("BCHM", "Biochemistry"),
("SOC", "Sociology"),
("AGEC", "Agricultural Economics"),
("ME", "Mechanical Engineering"),
("HTM", "Hospitality &amp; Tourism Mgmt"),
("MFET", "Manufacturing Engr Tech"),
("ASL", "American Sign Language"),
("THTR", "Theatre"),
("ITAL", "Italian"),
("SLHS", "Speech, Lang&amp;Hear Science"),
("SFS", "Sustainable Food&amp;Farm Sys"),
("OLS", "Organiz Ldrshp&amp;Supervision"),
("GSLA", "Global Studies Lib Arts"),
("BCM", "Bldg Construct Mgmt Tech"),
("YDAE", "Youth Develop &amp; Ag Educ"),
("GRAD", "Graduate Studies"),
("JPNS", "Japanese"),
("ECE", "Electrical &amp; Computer Engr"),
("AD", "Art &amp; Design"),
("NRES", "Natural Res &amp; Environ Sci"),
("PHRM", "Pharmacy"),
("ECET", "Electrical&amp;Comp Engr Tech"),
("HK", "Health And Kinesiology"),
("CGT", "Computer Graphics Tech"),
("CNIT", "Computer &amp; Info Tech"),
("LC", "Languages and Cultures"),
("ARAB", "Arabic"),
("PHYS", "Physics");