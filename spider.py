from selenium.webdriver.support.ui import Select
from webdriverwrapper import Firefox
from bs4 import BeautifulSoup
from getpass import getpass, getuser
from multiprocessing.pool import Pool
from selenium.common.exceptions import InvalidSelectorException
from datetime import datetime
import html
import time
import json
import glob
import csv
import sys
import re
last_major = ''

def fixdate(date):
	return datetime.strptime(date + '/' + sys.argv[1].split()[1], "%m/%d/%Y").strftime('%m-%d-%Y')

def get_24hour(ampm):
	return datetime.strptime(ampm, "%I:%M %p").strftime("%H:%M")


def create_sql(filename, items, table, cols):
	sql = 'INSERT INTO %s (%s) VALUES %s;'
	columns = ', '.join(cols)
	values = []
	for item in items:
		list_item = list(item)
		for i in range(0, len(list_item)):
			temp = html.escape(list_item[i])
			list_item[i] = temp.center(len(temp) + 2, '"')
		tup = '(%s)' % ', '.join(list_item)
		values.append(tup)
	
	sql = sql % (table, columns, ',\n'.join(values))
	with open(filename, 'w') as file:
		file.write(sql)


def create_csv(filename, tuples, header=None):
	with open(filename, 'w') as file:
		csv_out = csv.writer(file)
		if header:
			csv_out.writerow(header)
		for row in tuples:
			csv_out.writerow(row)


def create_json(filename, dict):
	with open(filename, 'w') as file:
		file.write(json.dumps(dict, indent=2))


def get_terms(html):
	soup = BeautifulSoup(html, 'lxml')
	select = soup.find(id='term_input_id')
	options = select.find_all('option')
	terms = {}
	for option in options:
		text = option.get_text().split()
		#print(text)
		if len(text) > 1:
			text = '%s %s' % (text[0], text[1])
			value = option.get('value')
			terms[text] = value
	return terms


def get_majors(html):
	soup = BeautifulSoup(html, 'lxml')
	select = soup.find(id='subj_id')
	options = select.find_all('option')
	majors = {}
	for option in options:
		abbrev = option.get('value')
		text = option.get_text()
		text = text.split('%s-' % abbrev)[-1]
		majors[abbrev] = text
	return majors



def login(driver):
	#passw = getpass('pass:')
	driver.fill_out({
		'username': input('Username: '),
		'password': getpass('Password: ')
	})
	driver.get_elm(name='submit').click()


def go_to_registration(driver):
	driver.get('https://wl.mypurdue.purdue.edu/web/portal/registration')
	driver.get_elm(xpath='/html/body/div[2]/div[1]/div[2]/div/div[1]/div/div/div/section/div/div/div/div/div/div/div/div/div[2]/p/span/a[5]').click()
	driver.switch_to_window(driver.window_handles[1])
	driver.close_other_windows()


def download_terms(driver):
	driver.wait_for_element(name='p_term')
	terms = get_terms(driver.page_source)
	#create_json('terms_%s.json' % table_append, terms)
	create_sql('terms_%s.sql' % table_append, terms.items(), 'TERMS', ['term_string', 'term_id'])
	#create_csv('terms_%s.csv' % table_append, terms.items())
	return terms



def select_term(driver, terms):
	try:
		driver.implicitly_wait(10)
		time.sleep(5)
		driver.switch_to_alert().accept()
	except:
		pass
	driver.wait_for_element(id_='term_input_id')
	select_element = Select(driver.get_elm(id_='term_input_id'))
	select_element.select_by_value(terms[sys.argv[1]])
	driver.get_elms(css_selector='input[type="submit"]')[1].click()


def download_majors(driver):
	global table_append
	driver.wait_for_element(id_='subj_id')
	majors = get_majors(driver.page_source)
	#create_json('majors_%s.json' % table_append, majors)
	#create_csv('majors_%s.csv' % table_append, majors.items())
	create_sql('majors_%s.sql' % table_append, majors.items(), 'MAJORS_' + table_append, ['abbrev', 'major'])
	return majors


def select_majors(driver, major=None, last_major=None):
	driver.wait_for_element(id_='subj_id')
	select_element = Select(driver.get_elm(id_='subj_id'))
	if last_major:
		select_element.deselect_by_value(last_major)
		print('Deselect ' + last_major)
	if major:
		select_element.select_by_value(major)
		print('Select ' + major)
	else:
		for option in select_element.options:
			option.click()
	driver.get_elms(css_selector='input[type="submit"]')[1].click()


def download_course(html):
	soup = BeautifulSoup(html, 'lxml')
	course = {}
	try:
		cols = soup.find_all('td')
		#print('%s - %s - %s' % (cols[1].get_text(), cols[2].get_text(), cols[7].get_text()))
		course['crn'] = cols[1].get_text().strip().strip()
		if not course['crn']:
			return None
		course['department'] = cols[2].get_text().strip()
		course['course_number'] = cols[3].get_text().strip()
		course['section_number'] = cols[4].get_text().strip()
		course['campus'] = cols[5].get_text().strip()
		credit_parts = cols[6].get_text().strip().split('-')
		if len(credit_parts) > 1:
			course['credit_hours_minimum'] = credit_parts[0].strip()
			course['credit_hours_maximum'] = credit_parts[1].strip()
		else:
			course['credit_hours_minimum'] = course['credit_hours_maximum'] = credit_parts[0].strip()
		course['title'] = cols[7].get_text().strip()
		course['days'] = cols[8].get_text().strip()
		time_parts = cols[9].get_text().split('-')
		if len(time_parts) > 1:
			course['time_start'] = get_24hour(time_parts[0].strip())
			course['time_end'] = get_24hour(time_parts[1].strip())
		else:
			course['time_start'] = course['time_end'] = ""
		course['capacity_seats'] = cols[10].get_text().strip()
		course['filled_seats'] = cols[11].get_text().strip()
		course['remaining_seats'] = cols[12].get_text().strip()
		course['capacity_waitlist'] = cols[13].get_text().strip()
		course['filled_waitlist'] = cols[14].get_text().strip()
		course['remaining_waitlist'] = cols[15].get_text().strip()
		course['capacity_crosslist'] = cols[16].get_text().strip()
		course['filled_crosslist'] = cols[17].get_text().strip()
		course['remaining_crosslist'] = cols[18].get_text().strip()
		instructor = cols[19].get_text().strip().split()
		course['instructors'] = ' '.join(instructor)
		date_parts = cols[20].get_text().split('-')
		if date_parts:
			course['date_start'] = fixdate(date_parts[0].strip())
			course['date_end'] = fixdate(date_parts[1].strip())
		course['location'] = cols[21].get_text().strip()
		course['type'] = cols[22].get_text().strip()
		course['links'] = cols[23].get_text().strip()
		course['prerequisites'] = cols[24].get_text().strip()
		course['notes'] = cols[25].get_text().strip()
		course['division'] = cols[26].get_text().strip()
		course['term'] = sys.argv[1]
		return course
	except Exception as e:
		print(e)
		return {"html": html}


def download_course_htmls(driver):
	buttons = driver.get_elms(name='SUB_BTN')
	length = len(buttons)
	htmls = []
	for i in range(0, length):
		print('%d / %d' % (i + 1, length))
		button = buttons[i]
		course_num = button.get_elm(xpath='../input[@name="SEL_CRSE"]').get_attribute('value')
		print(course_num)
		button.click()
		driver.wait_for_element(css_selector='table.datadisplaytable')
		driver.execute_script("window.scrollTo(0, document.body.scrollHeight)")
		table = driver.get_elm(css_selector='table.datadisplaytable')
		rows = []
		try:
			rows = table.get_elms(css_selector='tr')
		except InvalidSelectorException:
			table = driver.get_elm(css_selector='table.datadisplaytable')
			rows = table.get_elms(css_selector='tr')
		if len(rows) > 1:
			del rows[0]
			del rows[0]
		for row in rows:
			htmls.append(row.get_attribute('outerHTML'))
		driver.execute_script("window.history.go(-1)")
		driver.wait_for_element(css_selector='table.datadisplaytable', timeout=60)
		buttons = driver.get_elms(name='SUB_BTN')
	return htmls


def target_major(driver, abbrev):
	global last_major
	select_majors(driver, major=abbrev, last_major=last_major)
	last_major = abbrev
	htmls = download_course_htmls(driver)
	with open('htmls/%s_%s.html' % (abbrev, sys.argv[1]), 'w') as file:
		file.write(json.dumps(htmls))
	driver.execute_script("window.history.go(-1)")

def main():
	global table_append
	table_append = sys.argv[1].strip().upper()[0:2] + sys.argv[1][-2:]
	print(table_append)
	driver = Firefox()
	driver.get('https://mypurdue.purdue.edu')
	login(driver)
	go_to_registration(driver)
	terms = download_terms(driver)
	select_term(driver, terms)
	majors = download_majors(driver)
	files = glob.glob('htmls/*_%s.html' % sys.argv[1])
	print('%d majors missing' % (len(majors.items()) - len(files)))
	pool = Pool(processes=500)
	for major in majors.keys():
		if not glob.glob('htmls/%s_%s.html' % (major, sys.argv[1])):
			target_major(driver, major)
			
	driver.close()
	course_htmls = []
	for file in files:
		with open(file, 'r') as file:
			htmls = json.loads(file.read())
			course_htmls.extend(htmls)
	courses = pool.map(download_course, course_htmls)
	print('Rows ' + str(len(course_htmls)))
	#with open('htmls/CS_Fall 2018.html', 'r') as file:
	#	htmls = json.loads(file.read())
	#	courses = []
	#	for html in htmls:
	#		courses.append(download_course(html))
	print('Before filter ' + str(len(courses)))
	courses = list(filter(lambda c: c is not None, courses))
	print('Returned ' + str(len(courses)))
	#create_json('courses_%s.json' % table_append, courses)
	course_tuples = []
	#availability_tuples
	course_headers = ['crn', 'department', 'course_number', 'section_number', 'campus', 'credit_hours_minimum', 'credit_hours_maximum', 'title', 'days', 'time_start', 'time_end', 'capacity_seats', 'filled_seats', 'remaining_seats', 'capacity_waitlist', 'filled_waitlist', 'remaining_waitlist', 'capacity_crosslist', 'filled_crosslist', 'remaining_crosslist', 'instructors', 'date_start', 'date_end', 'location', 'type', 'links', 'prerequisites', 'notes', 'division', 'term']
	for course in courses:
		course_tuple = []
		for header in course_headers:
			value = course.get(header, 'null')
			if not value:
				value = "null"
			course_tuple.append(value)
		course_tuples.append(tuple(course_tuple))
	create_sql('courses_%s.sql' % table_append, course_tuples, 'COURSES_%s' % table_append, course_headers)
	with open('create_table_templates.sql', 'r') as template:
		content = template.read()
		content = content.format(table_append)
		with open('create_tables_%s.sql' % table_append, 'w') as file:
			file.write(content)

if __name__ == "__main__":
	main()